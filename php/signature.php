<?php
	function wcs_unparse_url($parsed_url) { 
		$scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : ''; 
		$host     = isset($parsed_url['host']) ? $parsed_url['host'] : ''; 
		$port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : ''; 
		$user     = isset($parsed_url['user']) ? $parsed_url['user'] : ''; 
		$pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : ''; 
		$pass     = ($user || $pass) ? "$pass@" : ''; 
		$path     = isset($parsed_url['path']) ? $parsed_url['path'] : ''; 
		$query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : ''; 
		$fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : ''; 
		return "$scheme$user$pass$host$port$path$query$fragment"; 
	} 
  function wcs_sign($query, $algo, $key) {
		return $query . '&signature=' . urlencode(base64_encode(hash_hmac($algo, $query, $key, True)));
	}
	function wcs_signature($url, $algo, $key) {
		$parsed_url = parse_url($url);
		$signed_data = $parsed_url['query'];
		if ($signed_data != "") {
			$signed_data .= "&";
		}
		$signed_data .= 'hash=' . urlencode($algo);
		$signed_data .= '&timestamp=' . urlencode(str_replace('+0000', 'Z', gmdate(DATE_ISO8601)));
		echo str_replace('+0000', 'Z', gmdate(DATE_ISO8601)) . "\n";
		$signed_data .= '&nonce=' . rand();
		echo $signed_data . "\n";
		$parsed_url['query'] = wcs_sign($signed_data, $algo, $key);
		return wcs_unparse_url($parsed_url);
	}

	# $url = 'https://eservices.montpellier-agglo.com/user?format=json&NameID=_1234&orig=compte-citoyen.montpellier-agglo.com';
	# echo $url . "\n";
	# echo wcs_signature($url, 'sha256', 'coin') . "\n";
